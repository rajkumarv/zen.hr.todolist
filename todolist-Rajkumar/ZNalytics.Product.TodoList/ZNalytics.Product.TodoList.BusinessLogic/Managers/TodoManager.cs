﻿using System;
using System.Collections.Generic;
using AutoMapper;
using ZNalytics.Product.Common.BusinessLogic;
using ZNalytics.Product.Common.Factories;
using ZNalytics.Product.Common.Interfaces;
using ZNalytics.Product.Common.Service;
using ZNalytics.Product.TodoList.DataAccess.Interfaces;
using ZNalytics.Product.TodoList.DataAccess.Repositories;
using ZNalytics.Product.TodoList.DataAccess.RepositoryObjects;

namespace ZNalytics.Product.TodoList.BusinessLogic.Managers
{
    public class TodoManager : BuninessLogicManagerBase
    {
        protected ITodoRepository TodoRepository;

        private TodoManager(ServiceContext currentServiceContext) : base(currentServiceContext)
        {
        }

        public ITodoRepository GetTodoRepository()
        {
            return TodoRepository ?? CreateTodoRepositoryFromServiceContext(CurrentServiceContext);
        }
        public ITodoRepository CreateTodoRepositoryFromServiceContext(ServiceContext currentServiceContext)
        {
            if (currentServiceContext == null) throw new ArgumentNullException("currentServiceContext");
            return TodoRepository = RepositoryFactory<TodoRepository>.Create(CurrentServiceContext);
        }

        public ITodoRepository SetTodoRepository(ITodoRepository todoRepository)
        {
            if (todoRepository == null) throw new ArgumentNullException("todoRepository");
            return TodoRepository = todoRepository;
        }

        public IList<Todo>GetAllTodos()
        {
            return GetTodoRepository().GetAllTodos();
        }

        public Todo GetTodoById(Guid todoId)
        {
            if (todoId == null) throw new ArgumentNullException("todoId");
            if (todoId == Guid.Empty) throw new ArgumentOutOfRangeException("todoId cannot be Guid.Empty.");

            return GetTodoRepository().GetByTodoId(todoId);
        }

        public Todo InsertTodo(Models.Todo todo)
        {
            if (todo == null) throw new ArgumentNullException("todo");
            if (todo.TodoId != Guid.Empty) throw new ArgumentOutOfRangeException("todo", "todo.todoId must be Guid.Empty.");

            Mapper.CreateMap<Models.Todo, Todo>();
            var todoInsert = Mapper.Map<Todo>(todo);
            todoInsert.TodoId = Guid.NewGuid();
            return GetTodoRepository().InsertTodo(todoInsert);
        }

        public Todo UpdateTodoById(Guid todoId, Models.Todo todo)
        {
            if (todoId == null) throw new ArgumentNullException("todoId");
            if (todoId == Guid.Empty) throw new ArgumentOutOfRangeException("todoId cannot be Guid.Empty.");

            if (todo == null) throw new ArgumentNullException("todo");
            //if (todo.TodoId == Guid.Empty) throw new ArgumentOutOfRangeException("todo.TodoId cannot be Guid.Empty.");

            Mapper.CreateMap<Models.Todo, Todo>();
            return GetTodoRepository().UpdateByTodoId(todoId, Mapper.Map<Todo>(todo));
        }

        public int DeleteTodoById(Guid todoId)
        {
            if (todoId == null) throw new ArgumentNullException("todoId");
            if (todoId == Guid.Empty) throw new ArgumentOutOfRangeException("todoId cannot be Guid.Empty.");
            return GetTodoRepository().DeleteByTodoId(todoId);
        }
    }
}
