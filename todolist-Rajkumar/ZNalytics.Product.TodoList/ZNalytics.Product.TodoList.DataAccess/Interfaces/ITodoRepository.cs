﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZNalytics.Product.Common.Interfaces;
using ZNalytics.Product.TodoList.DataAccess.RepositoryObjects;

namespace ZNalytics.Product.TodoList.DataAccess.Interfaces
{
    public interface ITodoRepository : IRepository
    {
        Todo InsertTodo(Todo todo);
        List<Todo> GetAllTodos();
        Todo GetByTodoId(Guid todoId);
        Todo UpdateByTodoId(Guid todoId, Todo todo);
        int DeleteByTodoId(Guid todoId);
    }
}
