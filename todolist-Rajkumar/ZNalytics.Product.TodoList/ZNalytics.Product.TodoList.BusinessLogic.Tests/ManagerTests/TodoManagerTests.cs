﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using ZNalytics.Product.Common;
using ZNalytics.Product.Common.Factories;
using ZNalytics.Product.Common.Interfaces;
using ZNalytics.Product.Common.Service;
using ZNalytics.Product.TodoList.BusinessLogic.Managers;
using ZNalytics.Product.TodoList.DataAccess.Interfaces;
using ZNalytics.Product.TodoList.DataAccess.Repositories;
using ZNalytics.Product.TodoList.DataAccess.RepositoryObjects;

namespace ZNalytics.Product.TodoList.BusinessLogic.Tests.ManagerTests
{
    [TestClass]
    public class TodoManagerTests : ManagerTestBase
    {
        private static readonly Guid TestTodoId = Guid.NewGuid();
        private static readonly DateTimeOffset TestueDateTime = DateTimeOffset.UtcNow;

        [TestInitialize()]
        public override void OnSetup()
        {
            SetupMocks();
    
        }

        public override void SetupMocks()
        {
        
        }

        [TestCleanup()]
        public override void OnCleanup()
        {
            
        }

        private static Models.Todo GetTodoModel(string todoName, string todoContent, DateTimeOffset dueDateTime, Boolean isCompleted)
        {
            return new Models.Todo()
            {
                TodoId = TestTodoId,
                Name = todoName,
                Content = todoContent,
                DueDateTime = TestueDateTime,
                IsCompleted = isCompleted
            };
        }

        private static Todo GetTodoExpected(string todoName, string todoContent, DateTimeOffset dueDateTime, Boolean isCompleted)
        {
            return new Todo()
            {
                TodoId = TestTodoId,
                Name = todoName,
                Content = todoContent,
                DueDateTime = TestueDateTime,
                IsCompleted = isCompleted
            };
        }

        [TestMethod]
        public void GetAllTodos_Success()
        {
            // Data Setup
            var todoModel = new List<Models.Todo>
            {
                GetTodoModel("Todo1", "TodoContent1", DateTimeOffset.Now, true )

            };
            var todoExpected = new List<Todo>
            {
                GetTodoExpected("Todo1", "TodoContent1", DateTimeOffset.Now, true )

            };
            
            //Arrange
            var todoManagerFactory = BusinessLogicManagerFactory<TodoManager>.Create(TestServiceContext);
            var todoRepository = MockRepository.GenerateStub<ITodoRepository>();
            todoRepository.Stub(x => x.GetAllTodos()).Return(todoExpected);
            todoManagerFactory.SetTodoRepository(todoRepository);
            
            //Act
            var e = todoManagerFactory.GetAllTodos();

            //Assert
            Assert.IsNotNull(e);
            Assert.IsTrue(e.Count == 1);

        }

        [TestMethod]
        public void GetAllTodoById_Success()
        {
            // Data Setup
            var todoModel = GetTodoModel("Todo1", "TodoContent1", DateTimeOffset.Now, true );
            var todoExpected = GetTodoExpected("Todo1", "TodoContent1", DateTimeOffset.Now, true);

            //Arrange
            var todoManagerFactory = BusinessLogicManagerFactory<TodoManager>.Create(TestServiceContext);
            var todoRepository = MockRepository.GenerateStub<ITodoRepository>();
            todoRepository.Stub(x => x.GetByTodoId(todoExpected.TodoId)).Return(todoExpected);
            todoManagerFactory.SetTodoRepository(todoRepository);

            //Act
            var e = todoManagerFactory.GetTodoById(todoModel.TodoId);

            //Assert
            Assert.IsNotNull(e);
            Assert.IsTrue(e.TodoId == todoExpected.TodoId);

        }

        [TestMethod]
        public void InsertTodo_Success()
        {
            // Data Setup
            var todoModel = GetTodoModel("Todo1", "TodoContent1", DateTimeOffset.Now, true);
            var todoExpected = GetTodoExpected("Todo1", "TodoContent1", DateTimeOffset.Now, true);

            todoModel.TodoId = Guid.Empty;
            //Arrange
            var todoManagerFactory = BusinessLogicManagerFactory<TodoManager>.Create(TestServiceContext);
            var todoRepository = MockRepository.GenerateStub<ITodoRepository>();
            todoRepository.Stub(x => x.InsertTodo(todoExpected)).Return(todoExpected).IgnoreArguments();
            todoManagerFactory.SetTodoRepository(todoRepository);

            //Act
            var e = todoManagerFactory.InsertTodo(todoModel);

            //Assert
            Assert.IsNotNull(e);
            Assert.AreEqual(e.Name, todoModel.Name);
            Assert.AreEqual(e.Content, todoModel.Content);
            Assert.AreEqual(e.DueDateTime, todoModel.DueDateTime);
            Assert.AreEqual(e.IsCompleted, todoModel.IsCompleted);
        }

        [TestMethod]
        public void UpdateTodoById_Success()
        {
            // Data Setup
            var todoModel = GetTodoModel("Todo1", "TodoContent1", DateTimeOffset.Now, true);
            var todoExpected = GetTodoExpected("Todo1", "TodoContent1", DateTimeOffset.Now, true);

            //Arrange
            var todoManagerFactory = BusinessLogicManagerFactory<TodoManager>.Create(TestServiceContext);
            var todoRepository = MockRepository.GenerateStub<ITodoRepository>();
            todoRepository.Stub(x => x.UpdateByTodoId(todoExpected.TodoId, todoExpected)).Return(todoExpected).IgnoreArguments();
            todoManagerFactory.SetTodoRepository(todoRepository);

            //Act
            var e = todoManagerFactory.UpdateTodoById(todoModel.TodoId, todoModel);

            //Assert
            Assert.IsNotNull(e);
            Assert.IsTrue(e.TodoId == todoModel.TodoId);
            Assert.AreEqual(e.Name, todoModel.Name);
            Assert.AreEqual(e.Content, todoModel.Content);
            Assert.AreEqual(e.DueDateTime, todoModel.DueDateTime);
            Assert.AreEqual(e.IsCompleted, todoModel.IsCompleted);

        }

        [TestMethod]
        public void DeleteTodoById_Success()
        {
            // Data Setup
            var todoModel = GetTodoModel("Todo1", "TodoContent1", DateTimeOffset.Now, true);
            var todoExpected = GetTodoExpected("Todo1", "TodoContent1", DateTimeOffset.Now, true);

            //Arrange
            var todoManagerFactory = BusinessLogicManagerFactory<TodoManager>.Create(TestServiceContext);
            var todoRepository = MockRepository.GenerateStub<ITodoRepository>();
            todoRepository.Stub(x => x.DeleteByTodoId(todoExpected.TodoId)).Return(1);
            todoManagerFactory.SetTodoRepository(todoRepository);

            //Act
            var e = todoManagerFactory.DeleteTodoById(todoModel.TodoId);

            //Assert
            Assert.IsNotNull(e);
            Assert.IsTrue(e == 1);

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void GetTodoById_Fails_Todo_IsEmpty()
        {
            // Data Setup

            //Arrange
            var todoManagerFactory = BusinessLogicManagerFactory<TodoManager>.Create(TestServiceContext);

            //Act
            var e = todoManagerFactory.GetTodoById(Guid.Empty);

            //Assert

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void InsertTodo_Fails_Todo_Null()
        {
            // Data Setup

            //Arrange
            var todoManagerFactory = BusinessLogicManagerFactory<TodoManager>.Create(TestServiceContext);

            //Act
            var e = todoManagerFactory.InsertTodo(null);

            //Assert

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void InsertTodo_Fails_TodoId_Not_Empty()
        {
            // Data Setup
            var todoModel = GetTodoModel("Todo1", "TodoContent1", DateTimeOffset.Now, true);

            //Arrange
            var todoManagerFactory = BusinessLogicManagerFactory<TodoManager>.Create(TestServiceContext);

            //Act
            var e = todoManagerFactory.InsertTodo(todoModel);

            //Assert

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void UpdateTodoById_Fails_Todo_Null()
        {
            // Data Setup

            //Arrange
            var todoManagerFactory = BusinessLogicManagerFactory<TodoManager>.Create(TestServiceContext);

            //Act
            var e = todoManagerFactory.UpdateTodoById(Guid.NewGuid(),  null);

            //Assert

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void UpdateTodoById_Fails_TodoId_Is_Empty()
        {
            // Data Setup
            var todoModel = GetTodoModel("Todo1", "TodoContent1", DateTimeOffset.Now, true);

            //Arrange
            var todoManagerFactory = BusinessLogicManagerFactory<TodoManager>.Create(TestServiceContext);

            //Act
            var e = todoManagerFactory.UpdateTodoById(Guid.Empty, todoModel);

            //Assert

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void DeleteTodoById_Fails_TodoId_Is_Empty()
        {
            // Data Setup

            //Arrange
            var todoManagerFactory = BusinessLogicManagerFactory<TodoManager>.Create(TestServiceContext);

            //Act
            var e = todoManagerFactory.DeleteTodoById(Guid.Empty);

            //Assert

        }
    }
}
