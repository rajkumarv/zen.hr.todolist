﻿using System;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using ZNalytics.Product.Common.Controllers;
using ZNalytics.Product.Common.Factories;
using ZNalytics.Product.TodoList.BusinessLogic.ManagerProxies;
using ZNalytics.Product.TodoList.BusinessLogic.Models;

namespace ZNalytics.Product.TodoList.API.Controllers
{
    public class TodoController : WebApiController
    {
        // GET api/Todo
        public HttpResponseMessage Get()
        {
            var todoManagerProxy = BusinessLogicManagerProxyFactory<TodoManagerProxy>.Create(FirstServiceContext);
            return todoManagerProxy.GetAllTodos();
        }

        // GET api/Todo/(Guid)
        public HttpResponseMessage Get(Guid id)
        {
            var todoManagerProxy = BusinessLogicManagerProxyFactory<TodoManagerProxy>.Create(FirstServiceContext);
            return todoManagerProxy.GetTodoById(id);
        }

        // POST api/Todo
        public HttpResponseMessage Post([FromBody]Todo todo)
        {
            var todoManagerProxy = BusinessLogicManagerProxyFactory<TodoManagerProxy>.Create(FirstServiceContext);
            return todoManagerProxy.InsertTodo(todo);
        }

        // PUT api/Todo/(Guid)
        public HttpResponseMessage Put(Guid id, [FromBody]Todo todo)
        {
            var todoManagerProxy = BusinessLogicManagerProxyFactory<TodoManagerProxy>.Create(FirstServiceContext);
            return todoManagerProxy.UpdateTodo(id,todo);
        }

        // DELETE api/Todo/(Guid)
        public HttpResponseMessage Delete(Guid id)
        {
            var todoManagerProxy = BusinessLogicManagerProxyFactory<TodoManagerProxy>.Create(FirstServiceContext);
            return todoManagerProxy.DeleteTodo(id);
        }
    }
}
