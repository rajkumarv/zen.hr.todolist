﻿

var todoApp = angular.module('todoApp', ['ngRoute', 'todoServices', 'ngTable']);

var todoEditdata = null;

todoApp.factory('apiService', ['$resource', function ($resource) {
                return $resource('http://localhost:3030/api/Todo/:id', {
                        id: '@id'
                    }, {
                        query: {
                            isArray: false
                        },
                        update: {
                            method: 'PUT'
                        }
                    });
    }
]);

// configure our routes
todoApp.config(function ($routeProvider) {
    $routeProvider

        // route for the home page
        .when('/', {
            templateUrl: 'pages/home.html',
            controller: 'mainController'
        })

        // route for the edit page
        .when('/edit', {
            templateUrl: 'pages/edit.html',
            controller: 'editController'
        });

});

// create the controller and inject Angular's $scope
todoApp.controller('mainController', ['$scope', '$location', 'apiService', function ($scope, $location, apiService) {
    // create a message to display in our view
    //console.log(todoEditdata);

    //todoServices.query(function (response) {
    //    // Assign the response INSIDE the callback
    //    console.log("GETALL======>");
    //    console.log(response.Data);
    //    $scope.data = response.Data;
    //});

    apiService.query(function (response) {
        // Assign the response INSIDE the callback
        console.log("GETALL======>");
        console.log(response.Data);
        $scope.data = response.Data;
    });

    $scope.modify = function (tableData) {
        console.log("SELECTED ITEM======>");
        console.log(tableData);
        todoEditdata = tableData;

        var t = apiService.get({ id: todoEditdata.TodoId }, function (response) {
            console.log("GETBYID===>");
            console.log(response.Data);
            $scope.data = response.Data;
        });


        $location.path('/edit');
    };

    //$scope.data = todoFactory.query(function (response) {
    //     //Assign the response INSIDE the callback
    //    console.log(response.Data);
    //    $scope.data = response.Data;
    //});
    
    

    //$scope.delete = function (tableData) {
    //    console.log("======>");
    //    console.log(tableData);

    //    var data = todoServices.get({ id: '962b8cea-689b-47d4-8c04-568a3161d20e' });
    //    console(data);

    //};

}]);

todoApp.controller('editController', ['$scope', '$location', 'apiService', function ($scope, $location, apiService) {
    $scope.message = 'Edit Todo';
    
    console.log("todoEditdata===>");
    console.log(todoEditdata.TodoId);

    if (todoEditdata != null) {
        console.log("GETBYID FROM EDIT PAGE BEGIN===>");
        console.log(apiService.get);

        var t = apiService.get({ id: todoEditdata.TodoId }, function (response) {
            console.log("GETBYID FROM EDIT PAGE RESULT PAGE ===>");
            console.log(response.Data);
            $scope.data = response.Data;
        });
    }
}]);

