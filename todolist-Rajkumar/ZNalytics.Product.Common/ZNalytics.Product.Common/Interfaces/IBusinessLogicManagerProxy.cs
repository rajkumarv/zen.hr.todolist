﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ZNalytics.Product.Common.Interfaces
{
    public interface IBusinessLogicManagerProxy
    {
        HttpResponseMessage CreateWebApiResponse(object data, object errors);
    }
}
