﻿using System;
using System.Reflection;
using ZNalytics.Product.Common.BusinessLogic;
using ZNalytics.Product.Common.Service;

namespace ZNalytics.Product.Common.Factories
{
    public static class BusinessLogicManagerProxyFactory<TBusinessLogicManagerProxy> where TBusinessLogicManagerProxy : BuninessLogicManagerProxyBase
    {
        public static TBusinessLogicManagerProxy Create(ServiceContext serviceContext)
        {
            if (serviceContext == null) throw new ArgumentNullException("serviceContext");

            const BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Instance;
            var ctor = typeof(TBusinessLogicManagerProxy).GetConstructor(flags, null, new Type[] { typeof(ServiceContext) }, null);

            return (TBusinessLogicManagerProxy)ctor.Invoke(new object[] { serviceContext });
        }
    }
}
