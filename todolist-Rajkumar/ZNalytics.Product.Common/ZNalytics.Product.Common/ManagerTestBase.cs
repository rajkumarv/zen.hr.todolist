﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZNalytics.Product.Common.Interfaces;
using ZNalytics.Product.Common.Service;

namespace ZNalytics.Product.Common
{
    public abstract class ManagerTestBase : IManagerTestBase
    {
        public ServiceContext TestServiceContext { get; set; }
        protected ManagerTestBase()
        {
            TestServiceContext = new ServiceContext();
            TestServiceContext.Load();
        }

        public abstract void OnSetup();
        public abstract void SetupMocks();
        public abstract void OnCleanup();
    }
}
