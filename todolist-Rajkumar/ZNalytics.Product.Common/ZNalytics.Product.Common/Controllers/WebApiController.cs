﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using PetaPoco;
using ZNalytics.Product.Common.DataAccess;
using ZNalytics.Product.Common.Service;

namespace ZNalytics.Product.Common.Controllers
{
    public class WebApiController : ApiController
    {
        public ServiceContext FirstServiceContext { get; set; }
        public WebApiController()
        {
            FirstServiceContext = new ServiceContext();
            FirstServiceContext.Load();

        }
    }
}
